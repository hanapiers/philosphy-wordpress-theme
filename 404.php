<?php
get_header();
?>
<!-- s-content
================================================== -->
<section class="s-content">
    <div class="row narrow">
        <div class="col-full s-content__header" data-aos="fade-up">
            <h1><?php _e('404 Not Found', 'philosophy') ?></h1>
            <p class="lead">
            <?php _e('The page you&rsquo;re looking for does not exists.<br>Perhaps searching can help.', 'philosophy') ?>
            </p>
        </div>

        <div class="col-full">
            <?php get_search_form(); ?>
        </div>
    </div>
</section> <!-- s-content -->

<?php get_footer() ?>
