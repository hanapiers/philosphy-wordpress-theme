<article class="row format-standard">

    <div class="s-content__header col-full">
        <?php the_title('<h1 class="s-content__header-title">', '</h1>') ?>

        <?php philosophy_header_meta() ?>
    </div> <!-- end s-content__header -->

    <?php if ( has_post_thumbnail() ): ?>
    <div class="s-content__media col-full">
        <div class="s-content__post-thumb align-center">
            <?php philosophy_post_thumbnail('standard_md') ?>
        </div>
    </div> <!-- end s-content__media -->
    <?php endif ?>

    <div class="col-full s-content__main">

        <?php the_content() ?>

        <?php philosophy_footer_meta() ?>
        <?php philosophy_author_card() ?>
        <?php philosophy_post_navigation() ?>  <!-- end s-content__pagenav -->
    </div> <!-- end s-content__main -->

</article>