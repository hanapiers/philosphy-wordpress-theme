<article class="masonry__brick entry format-standard" data-aos="fade-up">

    <div class="entry__thumb">
        <a href="<?php the_permalink(); ?>" class="entry__thumb-link">
            <?php philosophy_post_thumbnail('masonry_sm', array('style' => 'width: 100%;')) ?>
        </a>
    </div>

    <div class="entry__text">
        <div class="entry__header">
            <div class="entry__date">
                <a href="<?php the_permalink(); ?>"><?php esc_html_e(get_the_date()) // not the_date()  ?></a>
            </div>
            <?php the_title( '<h1 class="entry__title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h1>' ); ?>

        </div>
        <div class="entry__excerpt">
            <?php
            the_excerpt();
            ?>
        </div>
        <div class="entry__meta">
            <span class="entry__meta-links">
                <?php the_category(' ') ?>
            </span>
        </div>
    </div>

</article> <!-- end article -->
