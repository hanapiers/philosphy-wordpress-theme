<div id="post-<?php the_ID(); ?>" <?php post_class('row'); ?>>

    <div class="s-content__header col-full">
        <?php the_title('<h1 class="s-content__header-title">', '</h1>') ?>
    </div> <!-- end s-content__header -->

    <?php if ( has_post_thumbnail() ): ?>
    <div class="s-content__media col-full">
        <div class="s-content__post-thumb align-center">
        <?php philosophy_post_thumbnail('standard_md') ?>
        </div>
    </div> <!-- end s-content__media -->
    <?php endif ?>

    <div class="col-full s-content__main">
        <?php

        the_content();

        wp_link_pages( array(
            'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'wpdb' ),
            'after'  => '</div>',
        ) );

        ?>
    </div> <!-- end s-content__main -->

</div> <!-- end row -->