<div class="row masonry-wrap">
    <div class="masonry">

        <div class="grid-sizer"></div>

        <?php
        while ( have_posts() ):
            the_post();

            get_template_part('template-parts/masonry', 'grid');
        endwhile;
        ?>

    </div> <!-- end masonry -->
</div> <!-- end masonry-wrap -->

<?php philosophy_paginated_links(); ?>