<!DOCTYPE html>
<html class="no-js" lang="en">
<head>

    <!--- basic page needs
    ================================================== -->
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- mobile specific metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <?php wp_head() ?>

    <!-- favicons
    ================================================== -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

</head>

<body id="top" <?php body_class() ?>>

<!-- pageheader
================================================== -->
<section class="s-pageheader <?php (is_home()) ? esc_attr_e('s-pageheader--home') : '' ?>">

    <header class="header">
        <div class="header__content row">

            <div class="header__logo">
                <?php philosophy_the_logo() ?>
            </div> <!-- end header__logo -->

            <ul class="header__social">
                <li>
                    <a href="#0"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                </li>
                <li>
                    <a href="#0"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                </li>
                <li>
                    <a href="#0"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                </li>
                <li>
                    <a href="#0"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                </li>
            </ul> <!-- end header__social -->

            <a class="header__search-trigger" href="#0"></a>

            <div class="header__search">
                <form role="search" method="get" class="header__search-form" action="<?php echo esc_url( home_url( '/' ) ) ?>">
                    <label>
                        <span class="hide-content"><?php esc_html_e('Search for:', 'philosophy') ?></span>
                        <input type="search" class="search-field" placeholder="<?php esc_attr_e('Type Keywords', 'philosophy') ?>" value="" name="s" title="<?php esc_attr_e('Search for:', 'philosophy') ?>" autocomplete="off">
                    </label>
                    <input type="submit" class="search-submit" value="Search">
                </form>

                <a href="#0" title="Close Search" class="header__overlay-close"><?php esc_html_e('Close', 'philosophy') ?></a>

            </div>  <!-- end header__search -->


            <a class="header__toggle-menu" href="#0" title="Menu"><span><?php esc_html_e('Menu', 'philosophy') ?></span></a>

            <nav class="header__nav-wrap">

                <h2 class="header__nav-heading h6"><?php esc_html_e('Site Navigation', 'philosophy') ?></h2>

                <?php
                wp_nav_menu( array(
                    //'theme_location' => 'menu-1',
                    'menu_id'         => 'header-top-menu',
                    'menu_class'      => 'header__nav',
                    'container'       => '',
                    'container_class' => '',
                    'depth'           => 2,
                ) );
                ?> <!-- end header__nav -->

                <a href="#0" title="Close Menu" class="header__overlay-close close-mobile-menu"><?php esc_html_e('Close', 'philosophy') ?></a>

            </nav> <!-- end header__nav-wrap -->

        </div> <!-- header-content -->
    </header> <!-- header -->

    <?php
    if (is_home()) :
        get_template_part('template-parts/featured');
    endif;
    ?>
</section> <!-- end s-pageheader -->
