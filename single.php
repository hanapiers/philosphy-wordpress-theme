<?php get_header() ?>

<!-- s-content
================================================== -->
<section class="<?php philosophy_section_class() ?>">
    <?php

    while ( have_posts() ):
        the_post();

        get_template_part('template-parts/content');

        if ( comments_open() || get_comments_number() ) {
            comments_template();
        }

    endwhile; // End loop.

    ?>
</section> <!-- s-content -->


<?php get_footer() ?>
