<?php
get_header();
?>
<!-- s-content
================================================== -->
<section class="<?php philosophy_section_class() ?>">

    <?php if ( have_posts() ) { ?>
    <div class="row narrow">
        <div class="col-full s-content__header" data-aos="fade-up">
            <h1>
                <?php printf( __( 'Search Results for: %s', 'philosophy' ), '<span>' . get_search_query() . '</span>' ); ?>
            </h1>
        </div>
    </div>
    <?php get_template_part('template-parts/masonry'); ?>

    <?php } else  { ?>
    <div class="row narrow">
        <div class="col-full s-content__header" data-aos="fade-up">
            <h1>
                <?php printf( __( 'Search Results for: %s', 'philosophy' ), '<span>' . get_search_query() . '</span>' ); ?>
            </h1>
            <p class="lead"><?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'philosophy' ); ?></p>
        </div>
        <div class="col-full">
            <?php get_search_form(); ?>
        </div>
    </div>
    <?php } // endif ?>

</section> <!-- s-content -->

<?php get_footer() ?>
