<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ) ?>">
    <label>
        <span class="screen-reader-text"><?php _e( 'Search for:', 'label' ) ?></span>
        <input type="search" class="search-field full-width" placeholder="<?php  esc_attr_e( 'Search &hellip;', 'placeholder' )  ?>" value="<?php get_search_query() ?>" name="s" />
    </label>
    <input type="submit" class="btn btn--primary full-width" value="<?php esc_attr_e( 'Search', 'submit button' ) ?>" />
</form>