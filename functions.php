<?php

define('PHI_CSS_DIR', get_stylesheet_directory_uri() . '/assets/css/');
define('PHI_JS_DIR',  get_stylesheet_directory_uri() . '/assets/js/');
define('PHI_IMG_DIR', get_stylesheet_directory_uri() . '/assets/images/');
define('PHI_INC_DIR', get_stylesheet_directory() . '/inc/');
define('PHI_WGT_DIR', get_stylesheet_directory() . '/widgets/');
define('PHI_SB_COLS', array(
    /*===  1/3 + 2/3  ===*/
    'col_2_3' => array(
        'col-eight md-six tab-full',
        'col-four md-six tab-full'
    ),
    /*===  1/2 + 1/2  ===*/
    'col_1_2' => array(
        'col-six md-six tab-full',
        'col-six md-six tab-full'
    ),
    /*===  1/3 + 1/3 + 1/3  ===*/
    'col_1_3' => array(
        'col-four md-four tab-full',
        'col-four md-four tab-full',
        'col-four md-four tab-full'
    ),
    /*===  1/4 + 1/4 + 1/4 + 1/4  ===*/
    'col_1_4' => array(
        'col-three md-six tab-full',
        'col-three md-six tab-full',
        'col-three md-six tab-full',
        'col-three md-six tab-full',
    ),
    /*===  1/3 + 1/3 + 1/3 + 5/6  ===*/
    'col_1_5' => array(
        'col-two md-four mob-fulll',
        'col-two md-four mob-full',
        'col-two md-four mob-full',
        'col-five md-full end'
    ),
    /*===  1/6 + 1/6 + 1/6 + 1/2  ===*/
    'col_1_6' => array(
        'col-two md-four tab-full',
        'col-two md-four tab-full',
        'col-two md-four tab-full',
        'col-six md-full tab-full'
    )
));
define('PHI_EC_COLS', 'col_2_3');
define('PHI_FC_COLS', 'col_1_3');

/**
 * Load scripts
 */
function philosophy_load_assets() {
    /**
     * Wordpress specific files
     */
    wp_enqueue_style( 'philosophy-style', get_stylesheet_uri() );

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }

    /**
     * Theme specific
     */
    wp_enqueue_style( 'philosophy-base',    PHI_CSS_DIR . 'base.css' );
    wp_enqueue_style( 'philosophy-vendor',  PHI_CSS_DIR . 'vendor.css' );
    wp_enqueue_style( 'philosophy-main',    PHI_CSS_DIR . 'main.css' );
    wp_enqueue_style( 'philosophy-custom',  PHI_CSS_DIR . 'custom.css' );

    wp_enqueue_script( 'philosophy-modernzr', PHI_JS_DIR . 'modernizr.js' );
    wp_enqueue_script( 'philosophy-pace',     PHI_JS_DIR . 'pace.min.js' );
    wp_enqueue_script( 'philosophy-plugins',  PHI_JS_DIR . 'plugins.js', array('jquery'), null, true );
    wp_enqueue_script( 'philosophy-main',     PHI_JS_DIR . 'main.js', array('jquery'), null, true );

}

add_action( 'wp_enqueue_scripts', 'philosophy_load_assets' );


/**
 * Theme Setup
 */
if ( ! function_exists( 'philosophy_setup' ) ) {
    function philosophy_setup()
    {
        /*
         * Make theme available for translation.
         */
        load_theme_textdomain('philosophy_setup', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'menu-1' => esc_html__('Primary', 'philosophy'),
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('wpdb_custom_background_args', array(
            'default-color' => '19191b',
            'default-image' => '',
        )));

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        /**
         * Add support for core custom logo.
         */
        add_theme_support('custom-logo', array(
            //'height' => 48,
            'height' => 86,
            //'width' => 265,
            'width' => 478,
            'flex-width'  => false,
            'flex-height' => false
        ));

        add_image_size( 'standard_lg', 2000, 1000, true );
        add_image_size( 'standard_md', 1000, 500, true );
        add_image_size( 'standard_sm', 500, 250, true );

        add_image_size( 'masonry_sm', 400, 400, true );
        add_image_size( 'masonry_lg', 800, 800, true );

        add_image_size( 'masonry_gallery_sm', 400, 520, true );
        add_image_size( 'masonry_gallery_lg', 800, 1040, true );


        add_image_size( 'sticky_image', 1600, 1160, true );

        //add_theme_support( 'post-formats', array( 'video', 'audio', 'gallery' ) );

        // get from theme options

        $ec_col = get_theme_mod('phi_ec_col', PHI_EC_COLS); // extra content default
        $fc_col = get_theme_mod('phi_fc_col', PHI_FC_COLS); // footer content default

        foreach (PHI_SB_COLS[$ec_col] as $k => $col) {
            $idx = $k + 1;
            register_sidebar(array(
                'name'          => sprintf(__('Extra Content %d', 'philosophy'), $idx),
                'id'            => 'sbec-' . $idx,
                'description'   => __('Extra content before the footer. Check <strong>Appearance > Customize > Theme Options > Widget Area</strong>', 'philosophy'),
                'class'         => '',
                'before_widget' => '<div id="%1$s" class="wgt %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<h3>',
                'after_title'   => '</h3>'
            ));
        }

        register_sidebar(array(
            'name'          => __('Extra Content Bottom', 'philosophy'),
            'id'            => 'sbecb',
            'description'   => __('Extra bottom area content. Check <strong>Appearance > Customize > Theme Options > Widget Area</strong>', 'philosophy'),
            'class'         => '',
            'before_widget' => '<div id="%1$s" class="wgt %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h3>',
            'after_title'   => '</h3>'
        ));

        foreach (PHI_SB_COLS[$fc_col] as $k => $col) {
            $idx = $k + 1;
            register_sidebar(array(
                'name'          => sprintf(__('Footer Content %d', 'philosophy'), $idx),
                'id'            => 'sbfc-' . $idx,
                'description'   => __('Footer area content. Check <strong>Appearance > Customize > Theme Options > Widget Area</strong>', 'philosophy'),
                'class'         => '',
                'before_widget' => '<div id="%1$s" class="wgt %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<h4>',
                'after_title'   => '</h4>'
            ));
        }
    }
}
add_action( 'after_setup_theme', 'philosophy_setup' );

if ( ! function_exists('philosophy_customize_menu') ) {
    function philosophy_customize_menu($items) {
        foreach ($items as $i) {
            if ($i->current) {
                $i->classes[] = 'current';
            }

            if (! $i->menu_item_parent && in_array('menu-item-has-children', $i->classes)) {
                $i->classes[] = 'has-children';
            }

            if ($i->current_item_ancestor || $i->current_item_parent) {
                $i->classes[] = 'current';
            }
        }

        return $items;
    }
}


add_filter('wp_nav_menu_objects', 'philosophy_customize_menu');

function philosophy_set_post_views() {
    if ( ! is_single() && 'post' != get_post_type() ) return;

    $post_id = get_the_ID();
    $phi_pv_key = 'phi_post_view_num';

    $num = get_post_meta($post_id, $phi_pv_key, true);
    if( $num == '' ) {
        delete_post_meta($post_id, $phi_pv_key);
        add_post_meta($post_id, $phi_pv_key, '0');
    } else {
        $num++;
        update_post_meta($post_id, $phi_pv_key, $num);
    }
}

add_action( 'wp_head' , 'philosophy_set_post_views' );

if ( ! is_admin() ) {
    add_action( 'wp_print_scripts', 'no_mediaelement_scripts', 100 );
    add_filter( 'wp_video_shortcode_library', '__return_empty_string' );
    
    function no_mediaelement_scripts() {
        wp_dequeue_script( 'wp-mediaelement' );
        wp_deregister_script( 'wp-mediaelement' );
    }
}

require PHI_INC_DIR . 'template-tags.php';
require PHI_INC_DIR . 'customizer.php';
require PHI_WGT_DIR . 'widget-popular-posts.php';
require PHI_WGT_DIR . 'widget-about.php';
require PHI_WGT_DIR . 'widget-custom-archives.php';
require PHI_WGT_DIR . 'widget-list-links.php';
