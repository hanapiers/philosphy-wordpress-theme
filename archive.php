<?php
get_header();

?>
<!-- s-content
================================================== -->
<section class="<?php philosophy_section_class() ?>">
    <div class="row narrow">
        <div class="col-full s-content__header" data-aos="fade-up">
            <?php philosophy_archive_header() ?>
        </div>
    </div>

    <?php
    if (have_posts()):
        get_template_part('template-parts/masonry');
    else:
        get_template_part('template-parts/content', 'none');
    endif;
    ?>
</section> <!-- s-content -->

<?php get_footer() ?>
