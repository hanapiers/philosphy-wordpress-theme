<?php
if ( ! get_theme_mod('phi_fc_enable', true) ) {
    return;
}

$has_active_fc_widget = false;
$fc_col = get_theme_mod('phi_ef_col', PHI_FC_COLS);
foreach (PHI_SB_COLS[$fc_col] as $k => $col) {
    $id = 'sbfc-'. ($k + 1);
    if ( is_active_sidebar( $id ) ) {
        $has_active_fc_widget = true;
    }
}
?>

<div class="s-footer__main">
    <div class="row">

        <?php if ( ! $has_active_fc_widget ): ?>

        <div class="col-two md-four mob-full s-footer__sitelinks">

            <h4>Quick Links</h4>

            <ul class="s-footer__linklist">
                <li><a href="#0">Home</a></li>
                <li><a href="#0">Blog</a></li>
                <li><a href="#0">Styles</a></li>
                <li><a href="#0">About</a></li>
                <li><a href="#0">Contact</a></li>
                <li><a href="#0">Privacy Policy</a></li>
            </ul>

        </div> <!-- end s-footer__sitelinks -->

        <div class="col-two md-four mob-full s-footer__archives">

            <h4>Archives</h4>

            <ul class="s-footer__linklist">
                <li><a href="#0">January 2018</a></li>
                <li><a href="#0">December 2017</a></li>
                <li><a href="#0">November 2017</a></li>
                <li><a href="#0">October 2017</a></li>
                <li><a href="#0">September 2017</a></li>
                <li><a href="#0">August 2017</a></li>
            </ul>

        </div> <!-- end s-footer__archives -->

        <div class="col-two md-four mob-full s-footer__social">

            <h4>Social</h4>

            <ul class="s-footer__linklist">
                <li><a href="#0">Facebook</a></li>
                <li><a href="#0">Instagram</a></li>
                <li><a href="#0">Twitter</a></li>
                <li><a href="#0">Pinterest</a></li>
                <li><a href="#0">Google+</a></li>
                <li><a href="#0">LinkedIn</a></li>
            </ul>

        </div> <!-- end s-footer__social -->

        <div class="col-five md-full end s-footer__subscribe">

            <h4>Our Newsletter</h4>

            <p>Sit vel delectus amet officiis repudiandae est voluptatem. Tempora maxime provident nisi et fuga et enim exercitationem ipsam. Culpa consequatur occaecati.</p>

            <div class="subscribe-form">
                <form id="mc-form" class="group" novalidate="true">

                    <input type="email" value="" name="EMAIL" class="email" id="mc-email" placeholder="Email Address" required="">

                    <input type="submit" name="subscribe" value="Send">

                    <label for="mc-email" class="subscribe-message"></label>

                </form>
            </div>

        </div> <!-- end s-footer__subscribe -->

        <?php else:

            foreach (PHI_SB_COLS[$fc_col] as $k => $col):
                $idx = 'sbfc-' . ($k + 1);
                echo '<div class="' . esc_attr($col) . '">';
                if ( is_active_sidebar( $idx ) ) {
                    dynamic_sidebar( $idx );
                }
                echo '</div>';
            endforeach;

        endif; ?>

    </div>
</div> <!-- end s-footer__main -->