<?php get_sidebar(); ?>
<!-- s-footer
================================================== -->
<footer class="s-footer">

    <?php get_sidebar('footer'); ?>

    <div class="s-footer__bottom">
        <div class="row">
            <div class="col-full">
                <div class="s-footer__copyright">
                    <span><?php _e('© Copyright Philosophy 2018', 'philosophy') ?></span>
                    <span><?php _e('Site Template by <a href="https://colorlib.com/">Colorlib</a>', 'philosophy') ?></span>
                </div>

                <div class="go-top">
                    <a class="smoothscroll" title="Back to Top" href="#top"></a>
                </div>
            </div>
        </div>
    </div> <!-- end s-footer__bottom -->

</footer> <!-- end s-footer -->


<!-- preloader
================================================== -->
<div id="preloader">
    <div id="loader">
        <div class="line-scale">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
</div>


<!-- Java Script
================================================== -->
<?php wp_footer() ?>

</body>

</html>