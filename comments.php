<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package wpdb
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
    return;
}
?>
<!-- comments
================================================== -->
<div class="comments-wrap">

    <div id="comments" class="row">
        <div class="col-full">

            <?php if ( have_comments() ) : ?>
            <h3 class="h2">
                <?php
                $comment_count = get_comments_number();
                printf( _nx( '%s Comment', '%s Comments', $comment_count, 'philosophy' ), number_format_i18n( $comment_count ) );
                ?>
            </h3><!-- .h2.comments-title -->

            <!-- commentlist -->
            <ol class="commentlist">
                <?php
                wp_list_comments( array(
                    'style'      => 'ul',
                    'max_depth'  => 2,
                    'short_ping' => true,
                    'callback'   => 'philosophy_show_comments',
                ));
                ?>
            </ol><!-- .comment-list -->
            <?php endif; // Check for have_comments().?>

            <!-- respond
            ================================================== -->
            <?php philosophy_comment_form() ?><!-- end form -->

        </div> <!-- end col-full -->

    </div> <!-- end row comments -->
</div> <!-- end comments-wrap -->