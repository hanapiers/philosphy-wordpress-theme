<?php
if ( ! function_exists( 'philosophy_the_logo' ) ) {
    function philosophy_the_logo() {

        $img_src = PHI_IMG_DIR . '/logo.svg';

        if ( has_custom_logo() ) {
            $custom_logo_id = get_theme_mod( 'custom_logo' );
            $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
            $img_src = esc_url($logo[0]);
        }

        echo '<a class="logo" href="'. esc_attr(home_url()) .'">';
        echo '<img src="'. $img_src .'" alt="'.esc_attr(get_bloginfo('name')).'">';
        echo '</a>';
    }
}

if ( ! function_exists( 'philosophy_posted_on' ) ) {
    function philosophy_posted_on() {
        $time_string = '<time datetime="%1$s">%2$s</time>';
        if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
            $time_string = '<time datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
        }

        $time_string = sprintf( $time_string,
            esc_attr( get_the_date( DATE_W3C ) ),
            esc_html( get_the_date() ),
            esc_attr( get_the_modified_date( DATE_W3C ) ),
            esc_html( get_the_modified_date() )
        );

        return sprintf(
            esc_html_x( '%s', 'post date', 'philosophy' ),
            '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
        );
    }
}

if ( ! function_exists( 'philosophy_posted_by' ) ) {
    function philosophy_posted_by() {
        return sprintf(
            esc_html_x( 'By %s', 'post author', 'philosophy' ),
            '<a href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a>'
        );
    }
}

if ( ! function_exists( 'philosophy_header_meta' ) ) {
    function philosophy_header_meta() {
        echo '<ul class="s-content__header-meta">';

        if ( get_edit_post_link() ) {
            echo '<li class="edit-link">';
            philosophy_edit_link();
            echo '</li>';
        }

        echo '<li class="date">' . philosophy_posted_on() . '</li>';

        $categories_list = get_the_category_list(esc_html__(', ', 'philosophy'));
        if ($categories_list) {
            echo sprintf('<li class="cat">' . esc_html__('In %1$s', 'philosophy') . '</li>', $categories_list);
        }
        echo '</ul> <!-- end s-content__header-meta -->';
    }
}

if ( ! function_exists( 'philosophy_footer_meta' ) ) {
    function philosophy_footer_meta() {
        $tags_list = get_the_tag_list( '', esc_html_x( '', 'list item separator', 'philosophy' ) );
        if (! $tags_list) {
            return;
        }

        echo '<p class="s-content__tags">';
        echo '<span>'. esc_html__('Post Tags', 'philosophy') .'</span>';
        echo sprintf( '<span class="s-content__tag-list">' . $tags_list . '</span>', $tags_list );
        echo '</p> <!-- end s-content__tags -->';
    }
}

if ( ! function_exists( 'philosophy_author_card' ) ) {
    function philosophy_author_card() {

        $author_id = get_the_author_meta( 'ID' );

        $social_info = wp_get_user_contact_methods();

        $social_links = '';
        foreach ($social_info as $key => $social_label):
            $social_url = get_the_author_meta( $key );
            if ( ! $social_url ) continue;

            $social_links .='<li><a href="' . esc_url($social_url) . '">'. esc_html($social_label) .'</a></li>';
        endforeach;

        ?>

        <div class="s-content__author">
            <?php echo get_avatar( $author_id, 32 ) ?>
            <div class="s-content__author-about">
                <h4 class="s-content__author-name">
                    <a href="<?php echo esc_url( get_author_posts_url( $author_id ) )  ?>">
                        <?php the_author_meta('display_name') ?>
                    </a>
                </h4>

                <p><?php the_author_meta('description') ?></p>

                <ul class="s-content__author-social">
                    <?php echo $social_links; ?>
                </ul>
            </div>
        </div>

        <?php
    }
}

if ( ! function_exists( 'philosophy_edit_link' ) ) {

    function philosophy_edit_link() {
        edit_post_link(
            sprintf(
                wp_kses(
                    __('Edit <span class="screen-reader-text">%s</span>', 'wpdb'),
                    array(
                        'span' => array(
                            'class' => array(),
                        ),
                    )
                ),
                get_the_title()
            )
        );
    }
}

if ( ! function_exists( 'philosophy_post_thumbnail' ) ) {
    function philosophy_post_thumbnail($thumb_size, $attr = array()) {
        if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
            return;
        }

        $default_attr = array(
            'alt' => the_title_attribute( array(
                'echo' => false,
            )),
        );

        switch ($thumb_size) {
            case 'masonry_sm':
            case 'masonry_lg':
                $thumb_1x = wp_get_attachment_image_src( get_post_thumbnail_id(), 'masonry_sm' )[0];
                $thumb_2x = wp_get_attachment_image_src( get_post_thumbnail_id(), 'masonry_lg' )[0];
                $default_attr['srcset'] = sprintf('%s 1x, %s 2x', $thumb_1x, $thumb_2x);
                break;

            case 'masonry_gallery_sm':
            case 'masonry_gallery_lg':
                $thumb_1x = wp_get_attachment_image_src( get_post_thumbnail_id(), 'masonry_gallery_sm' )[0];
                $thumb_2x = wp_get_attachment_image_src( get_post_thumbnail_id(), 'masonry_gallery_lg' )[0];
                $default_attr['srcset'] = sprintf('%s 1x, %s 2x', $thumb_1x, $thumb_2x);
                break;

            case 'standard_sm':
            case 'standard_md':
            case 'standard_lg':
            $thumb_1x = wp_get_attachment_image_src( get_post_thumbnail_id(), 'standard_sm' )[0];
            $thumb_2x = wp_get_attachment_image_src( get_post_thumbnail_id(), 'standard_md' )[0];
            $thumb_3x = wp_get_attachment_image_src( get_post_thumbnail_id(), 'standard_lg' )[0];
            $default_attr['sizes'] = '(max-width: 2000px) 100vw, 2000px';
            $default_attr['srcset'] = sprintf('%s 2000w, %s 1000w, %s 500w', $thumb_3x, $thumb_2x, $thumb_1x);
                break;

            default:
                // it might be a small thumbnail so no sizes or srcset needed
                break;
        }

        $atts = wp_parse_args($attr, $default_attr);

        the_post_thumbnail( $thumb_size , $atts );
    }
}

if ( ! function_exists( 'philosophy_post_navigation' ) ) {
    function philosophy_post_navigation() {

        add_filter('navigation_markup_template', function(){
            return '<div class="s-content__pagenav navigation %1$s" role="navigation">
		    <h2 class="screen-reader-text">%2$s</h2>
		    <div class="s-content__nav">%3$s</div>
	        </div>';
        });

        add_filter('previous_post_link', function($output){
           return str_replace('nav-previous', 's-content__prev', $output);
        });

        add_filter('next_post_link', function($output){
            return str_replace('nav-next', 's-content__next', $output);
        });

        the_post_navigation(array(
            'prev_text'          => '<span>'.__('Previous Post', 'philosophy').'</span> %title',
            'next_text'          => '<span>'.__('Next Post', 'philosophy').'</span> %title',
            'in_same_term'       => false,
            'excluded_terms'     => '',
            'taxonomy'           => 'category',
            'screen_reader_text' => __( 'Post navigation' ),
        ));
    }
}

if ( ! function_exists( 'philosophy_paginated_links' ) ) {
    function philosophy_paginated_links() {

        add_filter('navigation_markup_template', function(){
            return '<div class="row">
                    <div class="col-full">
                        <nav class="pgn">%3$s</nav>
                    </div>
                    </div>';
        });

        $page_links = get_the_posts_pagination( array(
            'end_size'  => 1,
            'mid_size'  => 1,
            'prev_next' => true,
            'prev_text' => __('Prev', 'philosophy'),
            'next_text' => __('Next', 'philosophy'),
            'type'      => 'list'
        ) );

        $search = array(
            '<ul class=\'page-numbers\'>',
            'prev page-numbers',
            'next page-numbers',
            'page-numbers'
        );

        $replace = array(
            '<ul>',
            'pgn__prev',
            'pgn__next',
            'pgn__num'
        );


        echo str_replace($search, $replace, $page_links);
    }
}

if ( ! function_exists( 'philosophy_comment_form' ) ) {
    function philosophy_comment_form() {

        add_action('comment_form_top', function(){
            echo '<fieldset>';
        });
        add_action('comment_form', function(){
            echo '</fieldset>';
        });

        add_filter('comment_form_fields', function($fields){

            $commenter = wp_get_current_commenter();
            $req      = get_option( 'require_name_email' );
            $html_req = ( $req ? " required='required'" : '' );

            $fields['author'] = '
                <div class="form-field">
                    <input id="author" name="author" type="text" class="full-width" placeholder="'.__( 'Name' ) .'" value="' . esc_attr( $commenter['comment_author'] ) . '" '.$html_req.'>
                </div>
            ';

            $fields['email'] = '
                <div class="form-field">
                    <input id="email" name="email" type="email" class="full-width" placeholder="'.__( 'Email' ) .'" value="' . esc_attr( $commenter['comment_author_email'] ) . '" '.$html_req.'>
                </div>
            ';

            $fields['url'] = '
                <div class="form-field">
                    <input id="url" name="url" type="url" class="full-width" placeholder="'.__( 'Website' ) .'" value="' . esc_attr( $commenter['comment_author_url'] ) . '">
                </div>
            ';

            $fields['comment'] = '
                <div class="message form-field">
                    <textarea id="comment" name="comment" class="full-width" placeholder="' . _x( 'Comment', 'noun' ) . '" maxlength="65525" required="required"></textarea>
                </div>
            ';

            // rearrange field order
            $comment_field = $fields['comment'];
            unset( $fields['comment'] );
            $fields['comment'] = $comment_field;

            return $fields;
        });

        comment_form(array(
            'id_form' => 'contactForm',
            'title_reply_before' => '<h3 class="h2">',
            'title_reply_after' => '</h3>',
            'title_reply' => __('Add Comment', 'philosophy'),
            'cancel_reply_before'  => ' <small>',
            'cancel_reply_after'   => '</small>',
            'class_submit' => 'submit btn--primary btn--large full-width',
            'submit_field' => '%1$s %2$s'
        ));
    }
}

if ( ! function_exists( 'philosophy_archive_header' ) ) {
    function philosophy_archive_header() {
        add_filter('get_the_archive_description', function($desc) {
            return str_replace('<p>', '<p class="lead">', $desc);
        });

        the_archive_title( '<h1>', '</h1>' );
        the_archive_description();
    }
}

if ( ! function_exists( 'philosophy_section_class' ) ) {
    function philosophy_section_class() {

        $section_class = ['s-content'];
        if ( is_singular() ) { // single post, pages, custom post, attachment, etc.
            $section_class[] = 's-content--narrow';

            $has_comment_section = comments_open() || get_comments_number();

            if ( $has_comment_section && ! post_password_required() )  {
                $section_class[] = 's-content--no-padding-bottom';
            }
        }

        esc_attr_e(join(' ', $section_class));
    }
}

if ( ! function_exists( 'philosophy_show_comments' ) ) {
    function philosophy_show_comments( $comment, $args, $depth ) {
        $tag = 'li';
        $add_below = 'div-comment';
        if ( 'div' == $args['style'] ) {
            $tag = 'div';
            $add_below = 'comment';
        }

        ?>
        <<?php echo $tag; ?> <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
        <div class="comment__avatar">
            <?php if ( 0 != $args['avatar_size'] ) echo get_avatar( $comment, $args['avatar_size'] ); ?>
        </div>
        <?php if ( 'div' != $args['style'] ) : ?>
            <div class="comment__content" id="div-comment-<?php comment_ID(); ?>">
        <?php endif; ?>

        <?php if ( '0' == $comment->comment_approved ) : ?>
            <em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.' ) ?></em>
            <br />
        <?php endif; ?>

        <div class="comment__info">
            <cite><?php echo get_comment_author_link( $comment ) ?></cite>
            <div class="comment__meta">
                <a class="comment__time" href="<?php echo esc_url( get_comment_link( $comment, $args ) ); ?>">
                    <?php
                    /* translators: 1: comment date, 2: comment time */
                    printf( __( '%1$s at %2$s' ), get_comment_date( '', $comment ),  get_comment_time() ); ?>
                </a>
                <?php
                edit_comment_link( __( 'Edit' ), '&nbsp;&nbsp;', '&nbsp;&nbsp;' );

                add_filter('comment_reply_link', function($link) {
                    $output = str_replace('comment-reply-link', 'reply', $link);
                    return $output;
                });

                comment_reply_link( array_merge( $args, array(
                    'add_below' => $add_below,
                    'depth'     => $depth,
                    'max_depth' => $args['max_depth'],
                ) ) );

                ?>
            </div>
        </div>

        <div class="comment__text">
            <?php comment_text( $comment, array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
        </div>


        <?php if ( 'div' != $args['style'] ) : ?>
            </div>
        <?php endif; ?>
        <?php
    }
}

function feature_embeds() {
//    ! is_admin() && add_filter( 'the_content', function( $content )
//    {
//        // Get the avialable media items from the content
//        add_filter( 'media_embedded_in_content_allowed_types', 'wpse_media_types' );
//        $media = get_media_embedded_in_content( $content );
//        remove_filter( 'media_embedded_in_content_allowed_types', 'wpse_media_types' );
//
//        // Only use the first media item if available
//        if( $media )
//            $content = array_shift( $media );
//
//        return $content;
//    } , 99 );
//
//    function wpse_media_types() {
//        return [ 'audio', 'video', 'object', 'embed', 'iframe', 'img' ];
//    }
}

function philosophy_social_info( $contact_list ) {

    $contact_list['facebook'] = __( 'Facebook', 'philosophy' );

    $contact_list['twitter'] = __( 'Twitter', 'philosophy' );

    $contact_list['instagram'] = __( 'Instagram', 'philosophy' );

    return $contact_list;
}
add_filter( 'user_contactmethods', 'philosophy_social_info' );