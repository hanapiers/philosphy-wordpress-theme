<?php

include_once get_stylesheet_directory() .  '/inc/Acid/acid.php';
$acid = acid_instance(get_stylesheet_directory_uri() . '/inc/');

$data = array (
    'panels' => array (
        'phi-theme-option' => array (
            'title' => __( 'Theme Options', 'philosophy' ),
            'description' => __( 'Available customization options for Philosophy', 'philosophy' ),
            'sections' => array (
                'section-widget-setting' => array (
                    'title' => __( 'Widget Settings', 'philosophy' ),
                    'description' => __( 'Widget Area settings', 'philosophy' ),
                    'options' => array (
                        'phi-ec-cols' => array (
                            'label' => __( 'Widget Area Columns', 'philosophy' ),
                            'type' => 'radio-image',
                            'choices' => array (
                                array (
                                    'label' => __( '2 column', 'theme-slug' ),
                                    'url' => AcidConfig::assets_url() . 'images/smartcat-icon.jpg',
                                ),
                                array (
                                    'label' => __( '2 column 3 column', 'theme-slug' ),
                                    'url' => AcidConfig::assets_url() . 'images/smartcat-icon.jpg',
                                ),
                                array (
                                    'label' => __( 'guy running', 'theme-slug' ),
                                    'url' => AcidConfig::assets_url() . 'images/smartcat-icon.jpg',
                                ),
                            ),
                        ),
                        'demo-decimal1' => array (
                            'label' => __( 'Enter a Decimal', 'theme-slug' ),
                            'description' => __( 'Decimal', 'theme-slug' ),
                            'type' => 'range',
                            'default' => 10,
                            'min' => 0,
                            'max' => 100,
                            'step' => 5
                        ),
                        'demo-color' => array (
                            'label' => __( 'Pick a color', 'theme-slug' ),
                            'description' => __( 'Colorpicker option', 'theme-slug' ),
                            'type' => 'color',
                            'default' => __( '#cc0000', 'theme-slug' )
                        ),
                        'demo-text' => array (
                            'label' => __( 'Enter your title', 'theme-slug' ),
                            'description' => __( 'Create any text, HTML is not allowed', 'theme-slug' ),
                            'type' => 'text',
                            'default' => __( 'Created with Acid Framework', 'theme-slug' )
                        ),
                        'demo-image' => array (
                            'label' => __( 'Upload an image', 'theme-slug' ),
                            'description' => __( 'Allow users to select an image or upload a new one', 'theme-slug' ),
                            'type' => 'image',
                        ),
                        'image-sample' => array (
                            'label' => __( 'select an image', 'theme-slug' ),
                            'type' => 'radio-image',
                            'choices' => array (
                                array (
                                    'label' => __( 'guy running', 'theme-slug' ),
                                    'url' => AcidConfig::assets_url() . 'images/smartcat-icon.jpg',
                                ),
                                array (
                                    'label' => __( 'couple', 'theme-slug' ),
                                    'url' => AcidConfig::assets_url() . 'images/smartcat-icon.jpg',
                                ),
                                array (
                                    'label' => __( 'guy running', 'theme-slug' ),
                                    'url' => AcidConfig::assets_url() . 'images/smartcat-icon.jpg',
                                ),
                            ),
                        ),
                        'demo-url' => array (
                            'label' => __( 'Enter a URL', 'theme-slug' ),
                            'description' => __( 'Descriptions are optional', 'theme-slug' ),
                            'type' => 'url',
                            'default' => 'https://acidframework.com'
                        ),
                        'demo-number' => array (
                            'label' => __( 'Enter a Number', 'theme-slug' ),
                            'description' => __( 'Numeric value only', 'theme-slug' ),
                            'type' => 'number',
                            'default' => 10,
                            'min' => 0,
                            'max' => 100,
                            'step' => 5
                        ),
                        'demo-range' => array (
                            'label' => __( 'Enter a Number', 'theme-slug' ),
                            'description' => __( 'Numeric value only', 'theme-slug' ),
                            'type' => 'range',
                            'default' => 10,
                            'min' => 0,
                            'max' => 100,
                            'step' => 5
                        ),
                        'demo-textarea' => array (
                            'label' => __( 'Enter text', 'theme-slug' ),
                            'description' => __( 'Create any text, HTML is not allowed', 'theme-slug' ),
                            'type' => 'textarea',
                            'default' => __( 'Created with Acid Framework', 'theme-slug' )
                        ),
                        'demo-date' => array (
                            'label' => __( 'Enter a date', 'theme-slug' ),
                            'type' => 'date',
                            'default' => '2018-03-16'
                        ),
                        'demo-checkbox' => array (
                            'label' => __( 'Yes or no ?', 'theme-slug' ),
                            'description' => __( 'Use this control for options that are togglelable', 'theme-slug' ),
                            'type' => 'checkbox',
                            'default' => false
                        ),
                        'demo-toggle' => array (
                            'label' => __( 'On or Off ?', 'theme-slug' ),
                            'description' => __( 'Same as a checkbox, but looks more cool', 'theme-slug' ),
                            'type' => 'toggle',
                            'default' => true
                        ),
                        'demo-radio-toggle' => array (
                            'label' => __( 'Which option do you want?', 'theme-slug' ),
                            'description' => __( 'Radio toggle between various things', 'theme-slug' ),
                            'type' => 'radio-toggle',
                            'default' => true,
                            'choices' => array (
                                'one' => __( 'One', 'theme-slug' ),
                                'two' => __( 'Two', 'theme-slug' ),
                                'three' => __( 'Three', 'theme-slug' ),
                            ),
                        ),
                        'demo-color-picker' => array (
                            'label' => __( 'Select a color', 'theme-slug' ),
                            'description' => __( 'Which color do you want?', 'theme-slug' ),
                            'type' => 'color-select',
                            'default' => true,
                            'choices' => array (
                                '#06AED5' => __( 'Blue 1', 'theme-slug' ),
                                '#086788' => __( 'Blue 2', 'theme-slug' ),
                                '#38618C' => __( 'Blue 3', 'theme-slug' ),
                                '#FFBF29' => __( 'Yellow 1', 'theme-slug' ),
                                '#FFF1D0' => __( 'Baige 1', 'theme-slug' ),
                                '#DD1C1A' => __( 'Red 1', 'theme-slug' ),
                                '#FF5964' => __( 'Red 2', 'theme-slug' ),
                            ),
                        ),
                        'demo-html' => array (
                            'label' => __( 'Enter HTML Content', 'theme-slug' ),
                            'type' => 'html',
                            'default' => __( 'Enter any content here', 'theme-slug' ),
                        ),
                        'demo-radio' => array (
                            'label' => __( 'Select one of many', 'theme-slug' ),
                            'description' => __( 'Limits user to one selection', 'theme-slug' ),
                            'type' => 'radio',
                            'default' => 'red',
                            'choices' => array (
                                'red' => __( 'Red', 'theme-slug' ),
                                'blue' => __( 'Blue', 'theme-slug' ),
                                'green' => __( 'Green', 'theme-slug' ),
                                'purple' => __( 'purple', 'theme-slug' ),
                            ),
                        ),
                        'demo-select' => array (
                            'label' => __( 'Select from dropdown', 'theme-slug' ),
                            'type' => 'select',
                            'default' => 'purple',
                            'choices' => array (
                                'red' => __( 'Red', 'theme-slug' ),
                                'blue' => __( 'Blue', 'theme-slug' ),
                                'green' => __( 'Green', 'theme-slug' ),
                                'purple' => __( 'purple', 'theme-slug' ),
                            ),
                        ),
                        'demo-pages' => array (
                            'label' => __( 'Select a page', 'theme-slug' ),
                            'type' => 'dropdown-pages',
                        ),
                        'demo-email' => array (
                            'label' => __( 'Enter email address', 'theme-slug' ),
                            'type' => 'email',
                            'default' => get_option( 'admin_email' ),
                        ),
                    ),
                ),
            ),
        ),
    ),
);

$acid->config( $data );