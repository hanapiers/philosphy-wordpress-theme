<?php
if ( ! get_theme_mod('phi_ec_enable', true) ) {
    return;
}

$has_active_ec_widget = false;
$ec_col = get_theme_mod('phi_ec_col', PHI_EC_COLS);
foreach (PHI_SB_COLS[$ec_col] as $k => $col) {
    $id = 'sbec-'. ($k + 1);
    if ( is_active_sidebar( $id ) ) {
        $has_active_ec_widget = true;
    }
}

?>

<!-- s-extra
================================================== -->
<section class="s-extra">

    <div class="row top">

    <?php if ( ! $has_active_ec_widget ): ?>
        <div class="col-eight md-six tab-full popular">
            <?php
                $wgt_title = __('Popular Posts', 'philosophy');

                the_widget(
                    'Philosophy_Popular_Post_Widget',
                    'num_posts=6&title='.$wgt_title,
                    array(
                        'before_title'=>'<h3>',
                        'after_title'=>'</h3>',
                        'before_widget' => '',
                        'after_widget' => ''
                    )
                );
            ?>
        </div> <!-- end popular -->

        <div class="col-four md-six tab-full about">
            <?php
            $wgt_title = __('About Philosophy', 'philosophy');

            the_widget(
                'Philosophy_About_Widget',
                sprintf(
                    'title=%s&content=%s&social_enable=off&facebook=%s&twitter=%s&instagram=%s',
                    $wgt_title,
                    'A simple about text.',
                    'https://facebook.com/',
                    'https://twitter.com/',
                    'https://instagram.com/'
                ),
                array(
                    'before_title'=>'<h3>',
                    'after_title'=>'</h3>',
                    'before_widget' => '',
                    'after_widget' => ''
                )
            );
            ?>
        </div> <!-- end about -->

    <?php else:

        foreach (PHI_SB_COLS[$ec_col] as $k => $col):
            $idx = 'sbec-' . ($k + 1);
            echo '<div class="' . esc_attr($col) . '">';
                if ( is_active_sidebar( $idx ) ) {
                    dynamic_sidebar( $idx );
                }
            echo '</div>';
        endforeach;

    endif; ?>

    </div> <!-- end row -->

    <?php if ( get_theme_mod('phi_qtags_enable', true) ): ?>
    <div class="row bottom tags-wrap">
        <div class="col-full text-center">
            <?php
            if ( is_active_sidebar( 'sbecb' ) ) :
                dynamic_sidebar( 'sbecb' );
            else:
            ?>
            <h3>Tags</h3>

            <div class="tagcloud">
                <a href="#0">Salad</a>
                <a href="#0">Recipe</a>
                <a href="#0">Places</a>
                <a href="#0">Tips</a>
                <a href="#0">Friends</a>
                <a href="#0">Travel</a>
                <a href="#0">Exercise</a>
                <a href="#0">Reading</a>
                <a href="#0">Running</a>
                <a href="#0">Self-Help</a>
                <a href="#0">Vacation</a>
            </div> <!-- end tagcloud -->
            <?php endif; ?>
        </div> <!-- end tags -->
    </div> <!-- end tags-wrap -->
    <?php endif; ?>

</section> <!-- end s-extra -->