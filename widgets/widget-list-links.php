<?php

class Philosophy_List_Links_Widget extends WP_Widget {

    function __construct() {
        parent::__construct(
            'philosophy_list_links_widget', // Base ID
            esc_html__( 'Philosophy: List Links', 'philosophy' ), // Name
            array( 'description' => esc_html__( 'Display custom list links.', 'philosophy' ), ) // Args
        );
    }


    public function widget( $args, $instance ) {
        echo $args['before_widget'];

        // Output widget title
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
        }

        $list_type = $instance['list_type'];

        // Output generated fields
        echo '<p>'.$instance['content'].'</p>';
        echo '<p>'.$instance['dropdown_enable'].'</p>';
        echo '<p>'.$instance['count_enable'].'</p>';
        echo '<p>'.$instance['num_posts'].'</p>';

        echo $args['after_widget'];
    }

    public function form( $instance ) {

        $instance = wp_parse_args( (array) $instance, array(
            'list_type' => 'nav_menu',
            'title' => '',
            'dropdown_enable' => 'off',
            'count_enable' => 'off',
        ) );
        ?>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'philosophy' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>">
        </p>

        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'list_type' ) ); ?>"><?php _e( 'List Type:', 'philosophy' ); ?></label>
            <select name="<?php echo esc_attr( $this->get_field_name( 'list_type' ) ); ?>" id="<?php echo esc_attr( $this->get_field_id( 'list_type' ) ); ?>" class="widefat">
                <option value="nav_menu"<?php selected( $instance['list_type'], 'nav_menu' ); ?>><?php _e('Navigation Menu'); ?></option>
                <option value="wp_archive"<?php selected( $instance['list_type'], 'wp_archive' ); ?>><?php _e('Post Archives'); ?></option>
            </select>
        </p>

        <p id="wpa_opt" style="display: none">
            <input class="checkbox" type="checkbox"<?php checked( $instance['dropdown_enable'] ); ?> id="<?php echo $this->get_field_id('dropdown_enable'); ?>" name="<?php echo $this->get_field_name('dropdown_enable'); ?>" /> <label for="<?php echo $this->get_field_id('dropdown_enable'); ?>"><?php _e('Display as dropdown'); ?></label>
            <br/>
            <input class="checkbox" type="checkbox"<?php checked( $instance['count_enable'] ); ?> id="<?php echo $this->get_field_id('count_enable'); ?>" name="<?php echo $this->get_field_name('count_enable'); ?>" /> <label for="<?php echo $this->get_field_id('count_enable'); ?>"><?php _e('Show post counts'); ?></label>
        </p>

        <?php

    }


    public function update( $new_instance, $old_instance ) {

    }
}


function philosophy_list_links_widget() {
    register_widget( 'Philosophy_List_Links_Widget' );
}

add_action( 'widgets_init', 'philosophy_list_links_widget' );