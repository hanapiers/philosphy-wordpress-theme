<?php

class Philosophy_Popular_Post_Widget extends WP_Widget {


    function __construct() {
        parent::__construct(
            'philosophy_popular_post_widget', // Base ID
            esc_html__( 'Philosophy: Popular Posts', 'philosophy' ), // Name
            array( 'description' => esc_html__( 'Display popular posts.', 'philosophy' ), ) // Args
        );
    }

    private $widget_fields = array(
        array(
            'label' => 'No. of Posts to Show',
            'id' => 'num_posts',
            'default' => '6',
            'type' => 'number',
        ),
        array(
            'label' => 'Show Featured Image',
            'id' => 'ft_img_enable',
            'default' => 'on',
            'type' => 'checkbox',
        ),
        array(
            'label' => 'Show Author',
            'id' => 'author_enable',
            'default' => 'on',
            'type' => 'checkbox',
        ),
        array(
            'label' => 'Show Date',
            'id' => 'date_enable',
            'default' => 'on',
            'type' => 'checkbox',
        ),
    );

    /**
     * Front-end display of widget
     */
    public function widget( $args, $instance ) {
        echo $args['before_widget'];

        // Output widget title
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
        }

        // Output generated fields
        $num_posts = $instance['num_posts'] ?: 6;

        $r  = new WP_Query( array(
            'posts_per_page' => (int) $num_posts,
            'ignore_sticky_posts' => true,
            'meta_key' => 'phi_post_view_num',
            'orderby' => 'meta_value_num',
            'order' => 'DESC'
        ) );

        if ( $r->have_posts() ) :
            echo '<div class="block-1-2 block-m-full popular__posts">';

            foreach ( $r->posts as $popular_post ) :
                $title = get_the_title($popular_post->ID);
                $post_title = ($title) ? $title : __( '(no title)', 'philosophy' );
                $post_url = esc_url(get_the_permalink($popular_post->ID));

                $timestamp = esc_attr( get_the_date( 'Y-m-d', $popular_post->ID ) );
                $post_date = esc_html( get_the_date( '', $popular_post->ID ) );

                $post_author_url = esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) );
                $post_author_name = esc_html( get_the_author() );

                $extra_style = '';
                if ( ! $instance['ft_img_enable'] ) {
                    $extra_style = 'style="padding-left: 0px"';
                }
                ?>


                <article class="col-block popular__post" <?php echo $extra_style ?>>
                    <?php if ( $instance['ft_img_enable'] && has_post_thumbnail($popular_post) ): ?>
                        <a href="<?php echo $post_url ?>" class="popular__thumb">
                            <?php echo get_the_post_thumbnail($popular_post) ?>
                        </a>
                    <?php endif; ?>
                    <h5><a href="<?php echo $post_url ?>"><?php echo $post_title ?></a></h5>
                    <section class="popular__meta">
                        <?php if ( $instance['author_enable'] ): ?>
                        <span class="popular__author"><span><?php _e('by', 'philosophy') ?></span> <a href="<?php echo $post_author_url ?>"><?php echo $post_author_name ?></a></span>
                        <?php endif; ?>

                        <?php if ( $instance['date_enable'] ): ?>
                        <span class="popular__date"><span><?php _e('on', 'philosophy') ?></span> <time datetime="<?php echo $timestamp ?>"><?php echo $post_date ?></time></span>
                        <?php endif; ?>
                    </section>
                </article>

                <?php
            endforeach;

            echo '</div> <!-- end popular_posts -->';
        endif;


        echo $args['after_widget'];
    }

    /**
     * Back-end widget fields
     */
    public function field_generator( $instance ) {
        $output = '';
        foreach ( $this->widget_fields as $widget_field ) {
            $widget_value = ! empty( $instance[$widget_field['id']] ) ? $instance[$widget_field['id']] : esc_html__( $widget_field['default'], 'philosophy' );
            switch ( $widget_field['type'] ) {
                case 'checkbox':
                    $output .= '<p>';
                    $output .= '<input class="checkbox" type="checkbox" '.checked( $widget_value, true, false ).' id="'.esc_attr( $this->get_field_id( $widget_field['id'] ) ).'" name="'.esc_attr( $this->get_field_id( $widget_field['id'] ) ).'" value="1">';
                    $output .= '<label for="'.esc_attr( $this->get_field_id( $widget_field['id'] ) ).'">'.esc_attr( $widget_field['label'], 'philosophy' ).'</label>';
                    $output .= '</p>';
                    break;
                default:
                    $output .= '<p>';
                    $output .= '<label for="'.esc_attr( $this->get_field_id( $widget_field['id'] ) ).'">'.esc_attr( $widget_field['label'], 'philosophy' ).':</label> ';
                    $output .= '<input class="widefat" id="'.esc_attr( $this->get_field_id( $widget_field['id'] ) ).'" name="'.esc_attr( $this->get_field_name( $widget_field['id'] ) ).'" type="'.$widget_field['type'].'" value="'.esc_attr( $widget_value ).'">';
                    $output .= '</p>';
            }
        }
        echo $output;
    }

    public function form( $instance ) {
        $title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( '', 'philosophy' );
        ?>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'philosophy' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
        <?php
        $this->field_generator( $instance );
    }

    /**
     * Sanitize widget form values as they are saved
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        foreach ( $this->widget_fields as $widget_field ) {
            switch ( $widget_field['type'] ) {
                case 'checkbox':
                    $instance[$widget_field['id']] = $_POST[$this->get_field_id( $widget_field['id'] )];
                    break;
                default:
                    $instance[$widget_field['id']] = ( ! empty( $new_instance[$widget_field['id']] ) ) ? strip_tags( $new_instance[$widget_field['id']] ) : '';
            }
        }
        return $instance;
    }
}

function philosophy_popular_post_widget() {
    register_widget( 'Philosophy_Popular_Post_Widget' );
}

add_action( 'widgets_init', 'philosophy_popular_post_widget' );