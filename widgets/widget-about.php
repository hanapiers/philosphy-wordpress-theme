<?php
class Philosophy_About_Widget extends WP_Widget {

    function __construct() {
        parent::__construct(
            'philosophy_about_widget', // Base ID
            esc_html__( 'Philosophy: About', 'philosophy' ), // Name
            array( 'description' => esc_html__( 'Display details about the blog.', 'philosophy' ), ) // Args
        );
    }

    private $widget_fields = array(
        array(
            'label' => 'Content',
            'id' => 'content',
            'type' => 'textarea',
            'default' => '',
        ),
        array(
            'label' => 'Show social media info',
            'id' => 'social_enable',
            'default' => 'on',
            'type' => 'checkbox',
        ),
        array(
            'label' => 'Facebook',
            'id' => 'facebook',
            'type' => 'text',
            'default' => '',
        ),
        array(
            'label' => 'Twitter',
            'id' => 'twitter',
            'type' => 'text',
            'default' => '',
        ),
        array(
            'label' => 'Instagram',
            'id' => 'instagram',
            'type' => 'text',
            'default' => '',
        ),
    );

    /**
     * Front-end display of widget
     */
    public function widget( $args, $instance ) {
        echo $args['before_widget'];

        // Output widget title
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
        }

        // Output generated fields
        echo '<p>'.$instance['content'].'</p>';

        if ( $instance['social_enable'] ):
        ?>
        <ul class="about__social">
            <li>
                <a href="<?php echo esc_url($instance['facebook']) ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
            </li>
            <li>
                <a href="<?php echo esc_url($instance['twitter']) ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
            </li>
            <li>
                <a href="<?php echo esc_url($instance['instagram']) ?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
            </li>
        </ul> <!-- end header__social -->

        <?php
        endif;

        echo $args['after_widget'];
    }

    public function field_generator( $instance ) {

        


        $output = '';
        foreach ( $this->widget_fields as $widget_field ) {
            $widget_value = ! empty( $instance[$widget_field['id']] ) ? $instance[$widget_field['id']] : esc_html__( $widget_field['default'], 'philosophy' );
            switch ( $widget_field['type'] ) {
                case 'checkbox':
                    $output .= '<p>';
                    $output .= '<input class="checkbox" type="checkbox" '.checked( $widget_value, true, false ).' id="'.esc_attr( $this->get_field_id( $widget_field['id'] ) ).'" name="'.esc_attr( $this->get_field_id( $widget_field['id'] ) ).'" value="1">';
                    $output .= '<label for="'.esc_attr( $this->get_field_id( $widget_field['id'] ) ).'">'.esc_attr( $widget_field['label'], 'philosophy' ).'</label>';
                    $output .= '</p>';
                    break;
                case 'textarea':
                    $output .= '<p>';
                    $output .= '<label for="'.esc_attr( $this->get_field_id( $widget_field['id'] ) ).'">'.esc_attr( $widget_field['label'], 'philosophy' ).':</label> ';
                    $output .= '<textarea class="widefat" id="'.esc_attr( $this->get_field_id( $widget_field['id'] ) ).'" name="'.esc_attr( $this->get_field_name( $widget_field['id'] ) ).'" rows="6" cols="6" value="'.esc_attr( $widget_value ).'">'.$widget_value.'</textarea>';
                    $output .= '</p>';
                    break;
                default:
                    $output .= '<p>';
                    $output .= '<label for="'.esc_attr( $this->get_field_id( $widget_field['id'] ) ).'">'.esc_attr( $widget_field['label'], 'philosophy' ).':</label> ';
                    $output .= '<input class="widefat" id="'.esc_attr( $this->get_field_id( $widget_field['id'] ) ).'" name="'.esc_attr( $this->get_field_name( $widget_field['id'] ) ).'" type="'.$widget_field['type'].'" value="'.esc_attr( $widget_value ).'">';
                    $output .= '</p>';
            }
        }
        echo $output;
    }

    public function form( $instance ) {
        $title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( '', 'philosophy' );
        ?>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'philosophy' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
        <?php
        $this->field_generator( $instance );
    }

    /**
     * Sanitize widget form values as they are saved
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        foreach ( $this->widget_fields as $widget_field ) {
            switch ( $widget_field['type'] ) {
                case 'checkbox':
                    $instance[$widget_field['id']] = $_POST[$this->get_field_id( $widget_field['id'] )];
                    break;
                default:
                    $instance[$widget_field['id']] = ( ! empty( $new_instance[$widget_field['id']] ) ) ? strip_tags( $new_instance[$widget_field['id']] ) : '';
            }
        }
        return $instance;
    }
}

function philosophy_about_widget() {
    register_widget( 'Philosophy_About_Widget' );
}
add_action( 'widgets_init', 'philosophy_about_widget' );